module.exports = {
  publicPath: '',
  lintOnSave: false,
  assetsDir: 'static',
  "devServer" : {
      compress: true,
      disableHostCheck: true
  }
};