import Vue from 'vue'
import VueRouter from 'vue-router'
import joinUs from '../views/joinUs.vue'

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name: 'join_us',
    component: joinUs
  },
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

export default router
